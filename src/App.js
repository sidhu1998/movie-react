import React, { Component } from 'react'
import Background from './component/Background'
import NavBar from './component/Navbar'
class App extends Component {

  render() {
    return (
      <div>
      <Background/>
      <NavBar/>
      </div>
    )
  }
}

export default App;
