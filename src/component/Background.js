import React, { Component } from "react";
import '../../src/Background.css';




export class Background extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Directors: [],
      MoviesOfDirector:[],
     
       
    }
  }
  componentDidMount =() => {
      fetch('http://localhost:5000/api/directors/')
      .then(res=>res.json())
      .then(data=>{
          console.log(data);
          this.setState({
          Directors: data
          });
      })
      .catch((error)=>console.log(error))
  }

  getMoviesOfParticuarDirector = (id) =>{

    fetch(`http://localhost:5000/api/directors/${id}/movies`)
    .then(res=>res.json())
    .then(data=>{
        console.log(data);
        this.setState({
          MoviesOfDirector: data
        });
    })
    .catch((error)=>console.log(error))
}
  
  render() {
    var { Directors} = this.state;
    var {MoviesOfDirector}=this.state;
    return (
        <div className="container">
          {/* directors */}
          <div className="sidebar">
            <ul>
            {
              Directors.map(item => (
                  <li className="directorItem" key={item.directorId} onClick={() => this.getMoviesOfParticuarDirector(item.directorId)} >
                    {item.Name}
                  </li>
            ))}
            </ul>
          </div>
          {/* movies */}
          <div className="main">
          {
              MoviesOfDirector.map(item => (
                <div className="card" key={item.movie_id} >
                  {/* <h2>{item.movie_id}</h2>  */}
                  <span className="rank">{item.Movie_rank}</span> 
                  <h3>{item.title}</h3>
                  <br />
                  <span>{item.description}</span> 
                  <br />
                  <span>{item.runtime}</span> 
                  <br />
                  <span>{item.genre}</span> 
                  <br />
                  <span>{item.rating}</span> 
                  <br />
                  <span>{item.actor}</span> 
                </div>
            ))}
          </div>
        </div>
    )
  }
}
export default Background;
